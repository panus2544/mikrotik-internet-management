

<?php
   session_start();

      require '../ConnectToService.php';

            $debugingcode = false;

            if (isset($_SESSION['user_id'])) {
                $isLoggedin = true;
            } else {
                $isLoggedin = false;
            }

              if ($isLoggedin == true) {
              } elseif ($debugingcode === false) {
                  header("location:./");
              }

              if (!isset($_SESSION["AjaxFilterVerified"])) {
                  $_SESSION["AjaxFilterVerified"] = 0;
              }

      ?>


<!DOCTYPE html>
<html lang="en">
   <head>
      <title>ระบบจัดการอินเตอร์เน็ตวิทยาลัยการอาชีพปัว</title>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!--===============================================================================================-->
      <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
      <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
      <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
      <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
      <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
      <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="vendor/perfect-scrollbar/perfect-scrollbar.css">
      <!--===============================================================================================-->
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="css/util.css">
      <link rel="stylesheet" type="text/css" href="css/main.css">
      <link rel="stylesheet" type="text/css" href="css/mainLogin.css">


      <link rel="stylesheet" type="text/css" href="css/materialize.min.css">

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/materialize.min.js"></script>
      <!--===============================================================================================-->
      <script type="text/javascript" src="js/sweetalert.min.js"></script>
      <script>
         function reloadTableTag() {
           $("#DataTableBody").load(location.href + " #DataTableBody");
           UpdatedDatabase();
         }

         function UpdatedDatabase()
             {
               const toast = swal.mixin({
             toast: true,
             position: 'top-end',
             showConfirmButton: false,
             timer: 3000,
             });
             toast({
             type: 'success',
             title: 'อัปเดตข้อมูลการลงทะเบียน',
             })
             };

            function Notlogin() {
              swal({
                title: 'ผิดพลาด',
                text: 'กรุณาเข้าสู่ระบบก่อน',
                timer: 3000,
                showConfirmButton: false,
                allowOutsideClick: false,
                allowEscapeKey: false,
                type: 'error',
              }).then((result) => {
                if (
                  result.dismiss === swal.DismissReason.timer
                ) {
                  // ทำหลังจากเวลาหมด
                  window.location = "./";
                }
              })
            };

            function RefillData() {
              swal({
                title: 'ผิดพลาด',
                text: 'กรุณากรอกข้อมูลก่อน',
                timer: 3000,
                showConfirmButton: false,
                allowOutsideClick: false,
                allowEscapeKey: false,
                type: 'error',
              }).then((result) => {
                if (
                  result.dismiss === swal.DismissReason.timer
                ) {
                  // ทำหลังจากเวลาหมด
                  window.location = "./";
                }
              })
            };



            function WelcomeToSystem(){
              const toast = swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            });
            toast({
            type: 'success',
            title: 'ยินดีต้อนรับสู่ระบบจัดการ'
            })
          };

          function CannotConnectToservices() {
            swal({
              title: 'ไม่สามารถเชื่อมต่ออุปกรณ์ได้',
              text: 'กรุณาลองใหม่อีกครั้งภายหลัง',
              timer: 3000,
              allowEscapeKey: false,
              showConfirmButton: false,
              allowOutsideClick: false,
              type: 'error',
            }).then((result) => {
              if (
                result.dismiss === swal.DismissReason.timer
              ) {
              }
            })
          };

            function NotFounData() {
              swal({
                title: 'ไม่พบข้อมูล',
                text: 'กรุณาลองใหม่อีกครั้ง',
                confirmButtonText: 'ยืนยัน',
                timer: 3000,
                allowEscapeKey: false,
                showConfirmButton: false,
                allowOutsideClick: false,
                type: 'error',
              }).then((result) => {
                if (
                  result.dismiss === swal.DismissReason.timer
                ) {
                  // ทำหลังจากเวลาหมด
                  window.location = "./";
                }
              })
            };

                  document.getElementById("LogoutAdmin").onclick = LogoutPHP()




         function ConfrimAction(firstname, lastname, id) {
           Swal.fire({
             title: 'ยืนยันการลงทะเบียนให้กับ',
             text: firstname + "   " + "   " + lastname + " หรือไม่ ?",
             type: 'warning',
             showCancelButton: true,
             confirmButtonColor: '#25ad00',
             cancelButtonColor: '#3085d6',
             confirmButtonText: 'ยืนยัน',
             cancelButtonText: 'ยกเลิก',
           }).then((result) => {
             if (result.value) {
               SendAjaxAction(id, "Yes");
             }
           })
         }

         function DenyAction(firstname, lastname, id) {
           Swal.fire({
             title: 'ปฏิเสธการลงทะเบียนให้กับ',
             footer: 'การกระทำนี้ไม่สามารถย้อนกลับได้',
             text: firstname + "   " + "   " + lastname + " หรือไม่ ?",
             type: 'warning',
             showCancelButton: true,
             cancelButtonColor: '#3085d6',
             confirmButtonColor: '#d33',
             confirmButtonText: 'ยืนยัน',
             cancelButtonText: 'ยกเลิก',
           }).then((result) => {
             if (result.value) {
               SendAjaxAction(id, "No");
             }
           })
         }


         function SendAjaxAction(Ajax_identify_id, LocaIsConfrimAction){
           $.ajax({
            type: "POST",
            url: "PhpFunction.php",
            data: { ID_Passport : Ajax_identify_id ,IsConfrimAction : LocaIsConfrimAction}
          }).done(function( data ) {

            if(data == true){
                reloadTableTag();
            }else if(data == false){
                CannotConnectToservices();
            }
          });
         }

      </script>
      </body>
   </head>
   <body>

      <div class="limiter">
         <div class="container-table100">
            <div class="wrap-table100">
               <div class="table100">
                  <?php if ($debugingcode or $isLoggedin): ?>
                  <table>
                     <thead>
                        <tr class="table100-head" >
                           <td class="columnHeader">
                              <p>วิทยาลัยการอาชีพปัว</p>
                              <font size="3" color="white">
                                 <p>จัดทำโดย</p>
                                 <p>นายเมธี ตนะทิพย์</p>
                              </font>
                           </td>
                           <td class="column2"><i class='fa fa-id-card fa-5x' style='color:#ffffff;'></i></td>
                        </tr>
                     </thead>
                  </table>
                  <br>

                  <table>
                     <thead>
                        <tr class="table100-head">
                           <th class="column1Menu">
                           </th>
                           <th>
                              <span class="focus-input100"></span>
                              <span class="symbol-input100">
                              <i class="material-icons prefix"></i></i></span>
                              <div class="input-field col s6">
                                <i class="material-icons prefix">search</i>
                                <input id="SearchBoxID" type="text" class="validate" style="font-family: 'Kanit', sans-serif; color: white;" placeholder="ค้นหาข้อมูล">
                              </div>

<script>


  function AjaxFilterVerify() {

    var checkedValue = document.getElementById("IsVerified_Checkbox").checked;

      $.ajax({
       type: "POST",
       url: "AjaxCheckboxFilter.php",
       data: { checkedValue : checkedValue}
     }).done(function( data ) {

       reloadTableTag();
     });


  }
</script>

                           </th>
                           <th class="column3"><label>
                                <input id="IsVerified_Checkbox" type="checkbox" name="filterStatus" onchange="AjaxFilterVerify()"/>
                                <span>ลงทะเบียนสำเร็จแล้ว</span>
                              </label></th>
                           <th class="column3"><button class="login100-form-btn" onclick="reloadTableTag()" type="submit">อัปเดตข้อมูล</button></th>
                           <th class="column3"><button class="login100-form-btn" onclick="window.location.href='../AddAdminForm.php'" type="submit">เพิ่มข้อมูลผู้ดูแล</button></th>
                           <th class="column5logout"><button  name="LogoutAdmin" id="LogoutAdmin" class="login100-form-btn2" onclick="LogoutJava();">ออกระบบ</button></th>
                        </tr>
                     </thead>
                  </table>
                  <br>
                  <table name="DataTableBody" id="DataTableBody">
                     <thead>
                        <tr class="table100-head">
                           <th class="column1">ชื่อ</th>
                           <th class="column">นามสกุล</th>
                           <th class="column">เลขประจำตัว 13 หลัก</th>
                           <th class="column">อีเมล</th>
                           <th class="column">วันเกิด</th>
                           <th class="column">หลักสูตร</th>
                           <th class="column">ระดับชั้น</th>
                           <th class="column">เลขที่ห้อง</th>
                           <th class="column">สาขาวิชา</th>
                           <th class="column">โทรศัพย์</th>
                           <th class="column">ปีที่เข้ารับ<br>การศึกษา</th>
                           <th class="column" colspan="2">การยืนยัน</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           require '../ConnectToService.php';



                           $DepartmentSql = "SELECT * FROM `DepartmentManager` ";
                           $resultDepartment = $conn->query($DepartmentSql);

                           $CourseArray = array("ปวช", "ปวส");
                           $DepartmentArray = array();

                           if ($resultDepartment->num_rows > 0) {
                               while ($row = $resultDepartment->fetch_assoc()) {
                                   array_push($DepartmentArray, array($row["Department_ID"],$row["Department_Name"]));
                               }
                           }

                           //ชั่วคราวว
                           $IsVerifiedFilter = $_SESSION["AjaxFilterVerified"];

                           $SqlQueryUser = "SELECT * FROM `UserInformation` WHERE `IsVerified` = $IsVerifiedFilter";
                           $ResultUserQuery = $conn->query($SqlQueryUser);

                           $CountingNotVerify;

                           if ($ResultUserQuery->num_rows > 0) {
                               // output data of each row
                               while ($row = $ResultUserQuery->fetch_assoc()) {
                                   $BirthDayVar = substr_replace($row["BirthYears"], "/", 0, 0);
                                   $BirthDayVar = substr_replace($BirthDayVar, $row["BirthMonth"], 0, 0);
                                   $BirthDayVar = substr_replace($BirthDayVar, "/", 0, 0);
                                   $BirthDayVar = substr_replace($BirthDayVar, $row["BirthDate"], 0, 0);
                                   $Course = $CourseArray[$row["Course"]-1];
                                   $DepartmentName = $DepartmentArray[$row["Department"]-1][1];
                                   echo "<tr>
                                   <td class='column1'>$row[name]</td>
                                   <td class='column'>$row[lastname]</td>
                                   <td class='column'>$row[ID_Passport]</td>
                                   <td class='column'>$row[Email]</td>
                                   <td class='column'>$BirthDayVar</td>
                                   <td class='column'>$Course</td>
                                   <td class='column'>$row[StudentYear]</td>
                                   <td class='column'>$row[ClassNumber]</td>
                                   <td class='column'>$DepartmentName</td>
                                   <td class='column'>$row[PhoneNumber]</td>
                                   <td class='column'>$row[JoinYear]</td>
                                   <td class='column' value='$row[IsVerified]' style='display:none;'>$row[IsVerified]</td>
                                   ";
                                   $LocalIsVerified = $row["IsVerified"];
                                   if ($LocalIsVerified == 0) {
                                       echo '<td class="column"><button onclick="ConfrimAction(';
                                       echo "'";
                                       echo $row['name'];
                                       echo "'";
                                       echo ",";
                                       echo "'";
                                       echo $row['lastname'];
                                       echo "'";
                                       echo ",";
                                       echo $row['ID_Passport'];
                                       echo ')" class="fa fa-check fa-2x" style="color:#1ca332;cursor: pointer;"></button></td>';
                                       echo '
                                     <td class="column"><button onclick="DenyAction(';
                                       echo "'";
                                       echo $row['name'];
                                       echo "'";
                                       echo ",";
                                       echo "'";
                                       echo $row['lastname'];
                                       echo "'";
                                       echo ",";
                                       echo $row['ID_Passport'];
                                       echo ')" class="fa fa-times fa-2x" style="color:#a31c32;cursor: pointer;"></td></tr>';
                                   } else {
                                       echo '<td class="column"><i colspan="2" align="middle" class="fa fa-check fa-2x" style="color:#1ca332;"></i></td>';
                                   }
                               }
                           } else {
                               echo "<tr><td class='columnNotFound' colspan='12'><br>ข้อมูลการลงทะเบียนทั้งหมดได้รับการยืนยันแล้ว</td></tr>";
                               echo "<tr><td class='columnNotFound' colspan='12'><i class='fa fa-check-circle-o fa-5x' style='color:#1ca332;'></i></td></tr>";
                               echo "<tr><br></tr>";
                           }

                           ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>

      <script>
         document.querySelector('#SearchBoxID').addEventListener('keyup', filterTable, false);

           function filterTable(event) {
             var filter = event.target.value.toUpperCase();
             var rows = document.querySelector("#DataTableBody tbody").rows;

             for (var i = 0; i < rows.length; i++) {
                 var firstCol = rows[i].cells[0].textContent.toUpperCase();
                 var secondCol = rows[i].cells[1].textContent.toUpperCase();
                 var thirdcol = rows[i].cells[2].textContent.toUpperCase();
                 var fourcol = rows[i].cells[3].textContent.toUpperCase();
                 var fiftcol = rows[i].cells[5].textContent.toUpperCase();
                 var sixcol = rows[i].cells[8].textContent.toUpperCase();
                 if (firstCol.indexOf(filter) > -1 || secondCol.indexOf(filter) > -1 || thirdcol.indexOf(filter) > -1|| fourcol.indexOf(filter) > -1|| fiftcol.indexOf(filter) > -1|| sixcol.indexOf(filter) > -1) {
                     rows[i].style.display = "";
                 } else {
                     rows[i].style.display = "none";
                 }
             }
         }

         document.getElementById("LogoutAdmin").onclick = LogoutPHP()

         function LogoutSession() {
         $.ajax({
         url: 'logout.php'
         });
         }

         function LogoutJava()
         {
         swal({
         title: 'ออกจากระบบ',
         text: "คุณแน่ใจหรือไม่ที่จะออกจากระบบ",
         type: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: 'ใช่',
         allowEscapeKey: false,
         cancelButtonText: 'ไม่',
         }).then((result) => {
         if (result.value) {
         //ทำหลังจากยืนยัน
         LogoutSession();
         swal({
           title: 'ออกจากระบบแล้ว',
           timer: 3000,
           showConfirmButton: false,
           allowOutsideClick: false,
           allowEscapeKey: false,
           type: 'success',
         }).then((result) => {
           if (
             result.dismiss === swal.DismissReason.timer
           ) {
             // ทำหลังจากเวลาหมด
             window.location = "./";
           }
         })
         }
         })
         };
      </script>
      <?php endif; ?>
      <!--===============================================================================================-->
      <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
      <!--===============================================================================================-->
      <script src="vendor/bootstrap/js/popper.js"></script>
      <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
      <!--===============================================================================================-->
      <script src="vendor/select2/select2.min.js"></script>
      <!--===============================================================================================-->
      <script src="js/main.js"></script>
   </body>
</html>
