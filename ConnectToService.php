 <?php
// ข้อมูลการเชื่อมต่อ Database หรือฐานข้อมูล
$servername = "";
$username = "";
$password = "";
$dbname = "";
// ข้อมูลการเชื่อมต่อ Mikrotik
$MikrotikIp = ''; // Ip ของ Mikrotik
$MikrotikAdminUsername = ''; // Username ของ Mikrotik
$MiktorikAdminPassword = ''; // รหัสผ่าน ของ Mikrotik
$debugMode = false;// เปิดโหมด Debug เพื่อแสดงข้อมูลหรือไม่

// สร้างการเชื่อมต่อไปยัง Database หรือฐานข้อมูล

$conn = new mysqli($servername, $username, $password, $dbname);
mysqli_set_charset($conn, "utf8");

// ตรวจสอบการเชื่อมต่อ
if ($conn->connect_error) {
    die("เชื่อมต่อล้มเหลว : " . $conn->connect_error);
}

?>
