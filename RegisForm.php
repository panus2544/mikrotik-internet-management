<?php
session_start();


?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <!--  meta tags-->
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <!-- Title Page-->
      <title>ลงทะเบียนใช้งานอินเตอร์เน็ต</title>
      <!-- Icons font CSS-->
      <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
      <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
      <!-- Font special for pages-->
      <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
      <!-- Vendor CSS-->
      <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
      <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">
      <!-- Main CSS-->
      <link href="css/main.css" rel="stylesheet" media="all">
      <script type="text/javascript" src="js/sweetalert.min.js"></script>
   </head>

   <body>
      <div class="page-wrapper bg-gra-02 p-t-130 p-b-100">
         <div class="wrapper wrapper--w680">
            <div class="card card-4">
               <div class="card-body">
                 <center>
                  <h2 class="title">ลงทะเบียนใช้งานอินเตอร์เน็ตวิทยาลัยการอาชีพปัว</h2>
                </center>
                  <form method="post" action="./RegisterInternet.php" action="post" id="RegisterInternetIDForm" name="RegisterInterNet">
                     <div class="row row-space">
                        <div class="col-2">
                           <div class="input-group">
                              <label class="label">ชื่อ / Firstname </label>
                              <div class="input-group-icon">
                                 <input class="input--style-4" type="text" name="FirstName" id="FirstNameID" placeholder="กรุณากรอกชื่อ" required>
                                 <i class="fa fa-user input-icon"></i>
                              </div>
                           </div>
                        </div>
                        <div class="col-2">
                           <div class="input-group">
                              <label class="label">นามสกุล / Lastname</label>
                              <div class="input-group-icon">
                                 <input class="input--style-4" type="text" name="LastName" placeholder="กรุณากรอกนามสกุล" required>
                                 <i class="fa fa-user input-icon"></i>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row row-space">
                        <div class="col-2">
                          <div class="input-group">
                             <label class="label">เลขประจำตัว 13 หลัก / Passport ID</label>
                             <div class="input-group-icon">
                                <input onpaste="return false;" onkeypress="javascript:return isNumber(event)" maxlength="13" class="input--style-4" type="text" name="Identify" placeholder="กรุณากรอกรหัสประจำตัว 13 หลัก" required>
                                <i class="fa fa-address-card input-icon"></i>
                             </div>
                          </div>
                        </div>
                        <div class="col-2">
                           <div class="input-group">
                              <label class="label">อีเมล / Email</label>
                              <div class="input-group-icon">
                              <input class="input--style-4" type="email" name="Email" placeholder="กรุณากรอกอีเมล" required>
                              <i class="fa fa-envelope input-icon"></i>
                           </div>
                           </div>
                        </div>
                     </div>
                     <div class="row row-space">
                        <div class="col-2">
                           <div class="input-group">
                              <label class="label">หมายเลขโทรศัพท์ / Phone Number</label>
                              <div class="input-group-icon">
                              <input onpaste="return false;" onkeypress="javascript:return isNumber(event)" class="input--style-4" type="text" name="PhoneNumber" placeholder="กรุณากรอกหมายเลขโทรศัพย์" required>
                              <i class="fa fa-phone input-icon"></i>
                           </div>
                           </div>
                        </div>
                        <div class="col-2">
                           <div class="input-group">
                              <label class="label">วันเกิด / Date of birth</label>
                              <div class="input-group-icon">
                                 <input class="input--style-4 js-datepicker" type="text" name="Birthday" placeholder="กรุณาเลือกวันเดือนปีเกิด" required>
                                 <i class="fa fa-calendar input-icon js-btn-calendar"></i>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row row-space">
                        <div class="col-2">
                          <div class="input-group">
                            <label class="label">ชั้นปี / Student Year</label>
                            <div class="rs-select2 js-select-simple select--no-search">
                               <select name="StudentYear" required>
                                  <option disabled="disabled" selected="selected">กรุณาเลือกชั้นปี / Student Year</option>
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3">3</option>
                               </select>
                               <div class="select-dropdown"></div>
                            </div>
                          </div>
                        </div>
                        <div class="col-2">
                           <div class="input-group">
                             <label class="label">ห้อง / ClassNumber</label>
                             <div class="rs-select2 js-select-simple select--no-search">
                                <select name="ClassNumber" required>
                                   <option disabled="disabled" selected="selected">กรุณาเลือกห้อง / Class Number</option>
                                   <option value="1">1</option>
                                   <option value="2">2</option>
                                   <option value="3">3</option>
                                   <option value="4">4</option>
                                   <option value="5">5</option>
                                </select>
                                <div class="select-dropdown"></div>
                             </div>
                           </div>
                        </div>
                     </div>
                     <div class="row row-space">
                        <div class="col-2">
                          <div class="input-group">
                            <label class="label">ปีเข้ารับการศึกษา / Join Year</label>
                            <div class="rs-select2 js-select-simple select--no-search">
                               <select id="JoinYear" name="JoinYear" required>
                                  <option disabled="disabled" selected="selected">กรุณาเลือกปีเข้ารับการศึกษา / Join Year</option>
                               </select>
                               <div class="select-dropdown"></div>
                            </div>
                          </div>
                        </div>
                        <div class="col-2">
                           <div class="input-group">
                              <label class="label">หลักสูตร / Course</label>
                              <div class="rs-select2 js-select-simple select--no-search">
                                 <select name="Course" required>
                                    <option disabled="disabled" selected="selected">กรุณาเลือกหลักสูตร / Course</option>
                                    <option value="1">ประกาศนียบัตรวิชาชีพ (ปวช)</option>
                                    <option value="2">ประกาศนียบัตรวิชาชีพชั้นสูง (ปวส)</option>
                                 </select>
                                 <div class="select-dropdown"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="input-group">
                        <label class="label">แผนกวิชา / Department</label>
                        <div class="rs-select2 js-select-simple select--no-search">
                           <select id="DepartmentSelection" name="Department" required>
                              <option disabled="disabled" selected="selected">กรุณาเลือกแผนกวิชา / Department</option>
                           </select>
                           <div class="select-dropdown"></div>
                        </div>
                     </div>
                     <center>
                     <div class="p-t-15">
                        <button id="button" class="btn btn--radius-2 btn--blue" type="submit">ยืนยัน</button>
                      </center>
                     </div>
                     </div>
                     </div>
                     </div>
                     </div>
                    </form>
               </div>
            </div>
         </div>
      </div>
      <!-- Jquery JS-->
      <script src="vendor/jquery/jquery.min.js"></script>
      <!-- Vendor JS-->
      <script src="vendor/select2/select2.min.js"></script>
      <script src="vendor/datepicker/moment.min.js"></script>
      <script src="vendor/datepicker/daterangepicker.js"></script>
      <!-- Main JS-->
      <script src="js/global.js"></script>
   </body>
</html>
<script>
function UnlockButtonRecap(){
      document.getElementById('button').disabled = false;
}
</script>
<script>
    function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;

        return true;
    }
</script>

<script>
var currentTime = new Date()
var year = currentTime.getFullYear();

for (var i = 0; i < 5; i++) {
  var x = document.getElementById("JoinYear");
  var option = document.createElement("option");
  option.value = option.text = (year-i)+543;
  x.add(option);
}

</script>
<script>



   function AddDepartment(DepartmentName, DepartmentID) {
       var x = document.getElementById("DepartmentSelection");
       var option = document.createElement("option");
       option.value = DepartmentID;
       option.text = DepartmentName;
       x.add(option);
   }

</script>

<script>
$('#Identify').on('input', function (evt) {
var value = evt.target.value

if (value.length === 0) {
  evt.target.className = ''
  return
}

if ($.isNumeric(value)) {
  evt.target.className = 'input--style-4'
} else {
  evt.target.className = 'invalid'
}
})
</script>

<?php
   require './ConnectToService.php';

   $sql = "SELECT * FROM `DepartmentManager` ";

   $result = $conn->query($sql);

   if ($result->num_rows > 0) {
       $dom = new DOMDocument('1.0', 'utf-8');
       // output data of each row
       while ($row = $result->fetch_assoc()) {
           $optionid=$row['Department_ID'];
           $optionname=$row['Department_Name'];
           echo "<script type='text/javascript'> AddDepartment('$optionname', '$optionid',); </script>";
       }
   } else {
       echo "0 results";
   }
   $conn->close();
   ?>
